#ifndef lint
static const char yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93";
#endif

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20130925

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)

#define YYPREFIX "yy"

#define YYPURE 0

#line 37 "mas0.y"
#include <stdio.h>
#include <ctype.h>
#include "mas.h"

struct arg	arglist[3];
struct arg *ap	= { arglist};
struct	exp	explist[10];
struct	exp	*xp = { explist};
int	i, ii;
long	li;
extern struct	exp	usedot[];
struct	exp	*dotp = &usedot[0];
int	anyerrs;
int	passno	= 1;
FILE	*tmpfil;
FILE	*relfil;
FILE	*txtfil;
int	hshused;
long	tsize;
long	dsize;
long	bitfield;
int	bitoff;
int	usrname;
char	yytext[NCPS+2];
char	yystring[NCPS];
struct	nlist stabsym;
#line 68 "mas0.y"
	struct exp *pval;
#line 48 "y.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(void)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(msg)
#endif

extern int YYPARSE_DECL();

#define COLON 257
#define PLUS 258
#define MINUS 259
#define MUL 260
#define DIV 261
#define AMP 262
#define OFFOP 263
#define CM 264
#define NL 265
#define LB 266
#define RB 267
#define LP 268
#define RP 269
#define SEMI 270
#define INT 271
#define NAME 272
#define REG 273
#define NOCHAR 274
#define SPC 275
#define ALPH 276
#define DIGIT 277
#define SQ 278
#define SH 279
#define DQ 280
#define EXCL 281
#define EOR 282
#define IOR 283
#define STRING 284
#define SIZE 285
#define ISPACE 286
#define IBYTE 287
#define ILONG 288
#define INST0 289
#define INST1 290
#define INST2 291
#define ISHORT 292
#define IDATA 293
#define IGLOBAL 294
#define ISET 295
#define ITEXT 296
#define ICOMM 297
#define ILCOMM 298
#define IEVEN 299
#define IORG 300
#define IOPTIM 301
#define IPCREL 302
#define ISTABS 303
#define ISTABD 304
#define ISTABN 305
#define NOTYET 306
#define FILENAME 307
#define YYERRCODE 256
static const short yylhs[] = {                           -1,
    0,    7,    7,    7,    7,    9,    9,    8,   10,   10,
   10,   10,   10,   10,   10,   10,   10,   10,   10,   10,
   10,   10,   10,   10,   10,   10,   10,   10,   10,   10,
   11,   11,    4,    4,    1,    1,   13,   13,   13,   16,
   16,   12,   14,   15,    2,    2,    2,    2,    2,    2,
    2,    2,    2,    2,    3,    5,    6,    6,    6,    6,
    6,    6,    6,    6,    6,
};
static const short yylen[] = {                            2,
    1,    0,    3,    3,    3,    0,    3,    2,    4,    2,
    1,    2,    1,    2,    3,    3,    3,    2,    2,    4,
    1,    2,    3,    5,    3,    8,    6,    4,    1,    0,
    1,    3,    1,    0,    1,    1,    0,    3,    1,    1,
    3,    0,    0,    0,    1,    1,    3,    4,    4,    4,
    5,    7,    8,    2,    1,    1,    1,    1,    3,    3,
    3,    3,    3,    2,    2,
};
static const short yydefred[] = {                         2,
    0,    0,    0,    0,    0,    5,    3,    4,    0,    0,
   42,   43,    0,    0,    0,   44,    0,    0,    0,    0,
   35,   36,   21,    0,   29,    0,    0,    0,    0,    0,
    8,    7,    0,    0,   58,   57,    0,   18,    0,    0,
    0,   33,   22,    0,    0,    0,   12,   31,    0,    0,
   14,   19,    0,    0,    0,    0,    0,   64,    0,   65,
    0,    0,    0,    0,    0,    0,   39,    0,    0,    0,
    0,    0,   55,   23,   46,    0,    0,    0,    0,    0,
   25,    0,    0,    0,    0,   59,    0,    0,   62,   63,
    0,    0,    0,   54,    0,    0,    0,    0,   32,    9,
    0,   28,    0,   20,   41,   38,    0,    0,    0,    0,
   24,    0,    0,   48,    0,   49,    0,   50,    0,   27,
    0,   51,    0,    0,    0,    0,   26,    0,   52,   53,
};
static const short yydgoto[] = {                          1,
   30,   74,   75,   43,   65,   39,    2,    4,    5,   31,
   49,   40,   66,   41,   46,   67,
};
static const short yysindex[] = {                         0,
    0, -251, -252, -249, -171,    0,    0,    0, -234, -124,
    0,    0, -245, -245, -245,    0, -124, -236, -223, -124,
    0,    0,    0, -124,    0, -245, -232, -124, -124, -222,
    0,    0, -124, -124,    0,    0, -124,    0,  -71, -124,
 -124,    0,    0, -217, -217, -124,    0,    0, -205, -199,
    0,    0, -206, -195, -187, -186, -185,    0, -118,    0,
 -124, -124, -124, -124, -167, -184,    0, -184, -122, -124,
 -124, -244,    0,    0,    0, -175, -152, -184, -136, -124,
    0, -124, -124, -124, -124,    0, -235, -235,    0,    0,
 -124, -124, -244,    0, -130, -117, -134, -217,    0,    0,
 -119,    0, -108,    0,    0,    0, -111, -134,  -75, -225,
    0, -124, -124,    0, -216,    0, -134,    0,  -80,    0,
 -134,    0,  -93, -124,  -90,  -78,    0,  -76,    0,    0,
};
static const short yyrindex[] = {                         0,
    0,    1,    0,    0, -202,    0,    0,    0,    0,    0,
    0,    0, -173, -177, -177,    0,  -98,    0,    0,  -94,
    0,    0,    0,    0,    0,  -74,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0, -181, -253,
 -253,    0,    0,    0,    0, -253,    0,    0,  -92,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0, -157,  -91,    0,  -88,    0,    0,
    0,    0,    0,    0,    0, -102,    0,  -85,    0,    0,
    0,    0,    0,    0,    0,    0, -159, -104,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  -95,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
};
static const short yygindex[] = {                         0,
    0,  -41,  -50,   -6,  -10,  -31,    0,    0,    0,    0,
    0,    0,  140,    0,    0,  102,
};
#define YYTABLESIZE 306
static const short yytable[] = {                         38,
    1,   58,   59,   77,    3,   60,   47,   44,   45,   51,
   37,   37,    6,   52,   33,    7,   37,   55,   56,   53,
    8,   96,   32,   34,   63,   64,   35,   36,   73,   87,
   88,   89,   90,   76,   76,   48,   37,   58,  117,   42,
   59,   69,  107,  118,   70,   71,  110,  121,   50,   57,
   72,   54,  122,   35,   36,   73,  111,  115,   79,   94,
   95,   59,   30,   37,   80,   81,  123,   30,   82,  100,
  125,  101,  102,  103,  104,   56,   83,   84,   85,   92,
  105,   34,   56,   56,   34,   34,   56,   76,   56,   91,
   34,   34,   97,   34,   34,   34,   34,   60,   60,   60,
    9,  119,  120,   34,   60,   60,   40,   40,   60,   60,
   60,   98,   40,  127,   10,   11,   12,   13,   14,   15,
   16,   17,   18,   19,   20,   21,   22,   23,   24,   25,
   26,   27,   28,   29,   33,   99,   33,  108,   73,   61,
   62,   63,   64,   34,  112,   93,   35,   36,   35,   36,
   86,  109,   61,   61,   61,  113,   37,  114,   37,   61,
   61,   45,   45,   61,   61,   61,   11,   45,   47,   47,
   13,   11,   10,   15,   47,   13,   16,   10,   15,   17,
   68,   16,  116,  124,   17,   78,   61,   62,   63,   64,
  129,  126,  130,  106,  128,    0,    0,   34,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    6,    0,    0,    0,    0,
    6,    0,    6,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    6,    6,    6,    6,
    6,    6,    6,    6,    6,    6,    6,    6,    6,    6,
    6,    6,    6,    6,    6,    6,
};
static const short yycheck[] = {                         10,
    0,   33,   34,   45,  256,   37,   17,   14,   15,   20,
  264,  265,  265,   24,  259,  265,  270,   28,   29,   26,
  270,   72,  257,  268,  260,  261,  271,  272,  273,   61,
   62,   63,   64,   44,   45,  272,  281,   69,  264,  285,
   72,  259,   93,  269,  262,  263,   97,  264,  272,  272,
  268,  284,  269,  271,  272,  273,   98,  108,  264,   70,
   71,   93,  265,  281,  264,  272,  117,  270,  264,   80,
  121,   82,   83,   84,   85,  257,  264,  264,  264,  264,
   91,  259,  264,  265,  262,  263,  268,   98,  270,  257,
  268,  265,  268,  271,  272,  273,  270,  257,  258,  259,
  272,  112,  113,  281,  264,  265,  264,  265,  268,  269,
  270,  264,  270,  124,  286,  287,  288,  289,  290,  291,
  292,  293,  294,  295,  296,  297,  298,  299,  300,  301,
  302,  303,  304,  305,  259,  272,  259,  268,  273,  258,
  259,  260,  261,  268,  264,  268,  271,  272,  271,  272,
  269,  269,  257,  258,  259,  264,  281,  269,  281,  264,
  265,  264,  265,  268,  269,  270,  265,  270,  264,  265,
  265,  270,  265,  265,  270,  270,  265,  270,  270,  265,
   41,  270,  258,  264,  270,   46,  258,  259,  260,  261,
  269,  285,  269,   92,  285,   -1,   -1,  272,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  265,   -1,   -1,   -1,   -1,
  270,   -1,  272,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  286,  287,  288,  289,
  290,  291,  292,  293,  294,  295,  296,  297,  298,  299,
  300,  301,  302,  303,  304,  305,
};
#define YYFINAL 1
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 307
#if YYDEBUG
static const char *yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"COLON","PLUS","MINUS","MUL",
"DIV","AMP","OFFOP","CM","NL","LB","RB","LP","RP","SEMI","INT","NAME","REG",
"NOCHAR","SPC","ALPH","DIGIT","SQ","SH","DQ","EXCL","EOR","IOR","STRING","SIZE",
"ISPACE","IBYTE","ILONG","INST0","INST1","INST2","ISHORT","IDATA","IGLOBAL",
"ISET","ITEXT","ICOMM","ILCOMM","IEVEN","IORG","IOPTIM","IPCREL","ISTABS",
"ISTABD","ISTABN","NOTYET","FILENAME",
};
static const char *yyrule[] = {
"$accept : wholefile",
"wholefile : file",
"file :",
"file : file linstruction NL",
"file : file linstruction SEMI",
"file : file error NL",
"labels :",
"labels : labels NAME COLON",
"linstruction : labels instruction",
"instruction : ISET NAME CM expr",
"instruction : IGLOBAL names",
"instruction : IDATA",
"instruction : IDATA expr",
"instruction : ITEXT",
"instruction : ITEXT expr",
"instruction : IBYTE setchar explist",
"instruction : ILONG setint explist",
"instruction : ISHORT sethalf explist",
"instruction : ISPACE expr",
"instruction : IORG expr",
"instruction : comm NAME CM expr",
"instruction : IEVEN",
"instruction : INST0 optsize",
"instruction : INST1 optsize arg",
"instruction : INST2 optsize arg CM arg",
"instruction : IPCREL optsize NAME",
"instruction : ISTABS STRING CM expr CM expr CM expr",
"instruction : ISTABN expr CM expr CM expr",
"instruction : ISTABD expr CM expr",
"instruction : IOPTIM",
"instruction :",
"names : NAME",
"names : names CM NAME",
"optsize : SIZE",
"optsize :",
"comm : ICOMM",
"comm : ILCOMM",
"explist :",
"explist : explist CM outexpr",
"explist : outexpr",
"outexpr : expr",
"outexpr : expr COLON expr",
"setchar :",
"setint :",
"sethalf :",
"arg : expr",
"arg : reg",
"arg : LP reg RP",
"arg : MINUS LP reg RP",
"arg : LP reg RP PLUS",
"arg : expr LP reg RP",
"arg : OFFOP expr LP reg RP",
"arg : expr LP reg CM reg SIZE RP",
"arg : OFFOP expr LP reg CM reg SIZE RP",
"arg : AMP expr",
"reg : REG",
"expr : sexpr",
"sexpr : NAME",
"sexpr : INT",
"sexpr : LP sexpr RP",
"sexpr : sexpr PLUS sexpr",
"sexpr : sexpr MINUS sexpr",
"sexpr : sexpr MUL sexpr",
"sexpr : sexpr DIV sexpr",
"sexpr : MINUS sexpr",
"sexpr : EXCL sexpr",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    short    *s_base;
    short    *s_mark;
    short    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 512 "mas0.y"

/* VARARGS1 */
yyerror(s, a)
char *s;
{
	static char *lastfile = NULL;

	if (anyerrs==0)
		fprintf(stderr, "Assembler:\n");
	anyerrs++;
	fprintf(stderr, "line %d: ", lineno);
	fprintf(stderr, " (pass %d) ", passno);
	fprintf(stderr, s, a);
	if(lastfile != filename)
		fprintf(stderr, "\t(%s)", lastfile = filename);
	fprintf(stderr, "\n");
	yyerrok;
}
#line 415 "y.tab.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (short *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return -1;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return -1;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack)) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    yyerror("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 71 "mas0.y"
	{
		curlen = NBPW/2;
		dotp->xvalue &= ~01;
		flushfield(NBPW/2);
	}
break;
case 2:
#line 77 "mas0.y"
	{
		goto reset;
	}
break;
case 3:
#line 80 "mas0.y"
	{
		lineno++;
		goto reset;
	}
break;
case 4:
#line 84 "mas0.y"
	{
		goto reset;
	}
break;
case 5:
#line 87 "mas0.y"
	{
		lineno++;
		yyerrok;
	reset:
		ap = arglist;
		xp = explist;
		usrname = 0;
	}
break;
case 7:
#line 98 "mas0.y"
	{
		flushfield(NBPW/4);
		if ((yystack.l_mark[-1].yname->n_type&XTYPE)!=N_UNDF) {
			if((yystack.l_mark[-1].yname->n_type&XTYPE)!=dotp->xtype || yystack.l_mark[-1].yname->n_value!=dotp->xvalue
			 || passno==1 && yystack.l_mark[-1].yname->index != dotp->xloc) {
				yyerror("%.8s redefined", yystack.l_mark[-1].yname->n_name);
			}
		}
		yystack.l_mark[-1].yname->n_type &= ~(XTYPE|XFORW);
		yystack.l_mark[-1].yname->n_type |= dotp->xtype;
		yystack.l_mark[-1].yname->n_value = dotp->xvalue;
		if (passno==1)
			yystack.l_mark[-1].yname->index = dotp-usedot;
	}
break;
case 9:
#line 118 "mas0.y"
	{
		yystack.l_mark[-2].yname->n_type &= (N_EXT|XFORW);
		/*  what's this for?
		if($2->n_type&N_EXT && ($2->n_type&XTYPE)==N_UNDF && passno==2)
			$2->n_type &= ~N_EXT;
		*/
		yystack.l_mark[-2].yname->n_type |= yystack.l_mark[0].yexp->xtype&(XTYPE|XFORW);
		yystack.l_mark[-2].yname->n_value = yystack.l_mark[0].yexp->xvalue;
		if (passno==1)
			yystack.l_mark[-2].yname->index = yystack.l_mark[0].yexp->xloc;
		else if ((yystack.l_mark[0].yexp->xtype&XTYPE)==N_UNDF)
			yyerror("Illegal set");
	}
break;
case 11:
#line 132 "mas0.y"
	{
		i = -IDATA;
		goto chloc;
	}
break;
case 12:
#line 136 "mas0.y"
	{
		i = IDATA;
		goto chloc;
	}
break;
case 13:
#line 140 "mas0.y"
	{
		i = -ITEXT;
		goto chloc;
	}
break;
case 14:
#line 144 "mas0.y"
	{
		i = ITEXT;
	chloc:
		if (i < 0) {
			ii = 0;
			i = -i;
		} else {
			if (yystack.l_mark[0].yexp->xtype != N_ABS || (ii=yystack.l_mark[0].yexp->xvalue) >= NLOC) {
				yyerror("illegal location counter");
				ii = 0;
			}
		}
		if (i == IDATA)
			ii += NLOC;
		flushfield(NBPW/2);
		dotp = &usedot[ii];
		if (passno==2) {
			if (usefile[ii] == NULL) {
				tmpn2[TMPC] = 'a'+ii;
				if ((usefile[ii]=fopen(tmpn2, "w"))==NULL) {
					yyerror("cannot create temp");
					delexit();
				}
				tmpn3[TMPC] = 'a'+ii;
				if ((rusefile[ii]=fopen(tmpn3, "w"))==NULL) {
					yyerror("cannot create temp");
					delexit();
				}
			}
			txtfil = usefile[ii];
			relfil = rusefile[ii];
		}
	}
break;
case 15:
#line 177 "mas0.y"
	{
		flushfield(NBPW/4);
		if (bitoff)
			dotp->xvalue++;
	}
break;
case 16:
#line 182 "mas0.y"
	{
		flushfield(NBPW);
	}
break;
case 17:
#line 185 "mas0.y"
	{
		flushfield(NBPW/2);
	}
break;
case 18:
#line 188 "mas0.y"
	{
		if (yystack.l_mark[0].yexp->xtype != N_ABS)
			yyerror("space size not absolute");
		li = yystack.l_mark[0].yexp->xvalue;
	ospace:
		flushfield(NBPW/4);
		if (dotp->xvalue&01) {
			dotp->xvalue--;
			li--;
			curlen = NBPW/2;
			flushfield(NBPW/2);
		}
		while (li>1) {
			li -= 2;
			dotp->xvalue += 2;
			outhw(0, N_ABS, SNULL, 0);
		}
		if (li>0) {
			bitfield = 0;
			bitoff= NBPW/4;
			dotp->xvalue++;
		}
	}
break;
case 19:
#line 211 "mas0.y"
	{
		if (yystack.l_mark[0].yexp->xtype==N_ABS)
			orgwarn++;
		else if (yystack.l_mark[0].yexp->xtype!=dotp->xtype)
			yyerror("Illegal 'org'");
		li = yystack.l_mark[0].yexp->xvalue - dotp->xvalue;
		if (li < 0)
			yyerror("Backwards 'org'");
		goto ospace;
	}
break;
case 20:
#line 221 "mas0.y"
	{
		if (yystack.l_mark[0].yexp->xtype != N_ABS)
			yyerror("comm size not absolute");
		if (passno==1 && (yystack.l_mark[-2].yname->n_type&XTYPE)!=N_UNDF)
			yyerror("Redefinition of %.8s", yystack.l_mark[-2].yname->n_name);
		if (passno==1) {
			yystack.l_mark[-2].yname->n_value = yystack.l_mark[0].yexp->xvalue;
			if (yystack.l_mark[-3].yint==ICOMM)
				yystack.l_mark[-2].yname->n_type |= N_EXT;
			else {
				yystack.l_mark[-2].yname->n_type &= ~XTYPE;
				yystack.l_mark[-2].yname->n_type |= N_BSS;
			}
		}
	}
break;
case 21:
#line 236 "mas0.y"
	{
		flushfield(NBPW/2);
	}
break;
case 22:
#line 239 "mas0.y"
	{
		insout(yystack.l_mark[-1].yname, (struct arg *)NULL, (struct arg *)NULL, yystack.l_mark[0].yint);
	}
break;
case 23:
#line 242 "mas0.y"
	{
		insout(yystack.l_mark[-2].yname, yystack.l_mark[0].yarg, (struct arg *)NULL, yystack.l_mark[-1].yint);
	}
break;
case 24:
#line 245 "mas0.y"
	{
		insout(yystack.l_mark[-4].yname, yystack.l_mark[-2].yarg, yystack.l_mark[0].yarg, yystack.l_mark[-3].yint);
	}
break;
case 25:
#line 248 "mas0.y"
	{
		li = yystack.l_mark[0].yname->n_value - dotp->xvalue;
		if (yystack.l_mark[-1].yint==0)
			yystack.l_mark[-1].yint = W;
		if (passno == 2 && (dotp->xtype != yystack.l_mark[0].yname->n_type
		 || (yystack.l_mark[-1].yint==W && (li < -65536 || li > 65535))))
			yyerror("Illegal pcrel value");
		if (yystack.l_mark[-1].yint==B)
			yyerror("Illegal size");
		if (yystack.l_mark[-1].yint==L) {
			outhw((short)(li>>16), yystack.l_mark[0].yname->n_type, yystack.l_mark[0].yname, XPCREL+X2WDS);
			outhw((short)li, N_ABS, SNULL, 0);
		} else
			outhw((short)li, yystack.l_mark[0].yname->n_type, yystack.l_mark[0].yname, XPCREL);
		dotp->xvalue += 2;
	}
break;
case 26:
#line 264 "mas0.y"
	{
		nstabs++;
		if (passno==2) {
			strncpy(stabsym.n_name, yystack.l_mark[-6].ystring, NCPS);
			stabsym.n_dtype = ckabs(yystack.l_mark[-4].yexp);
			stabsym.n_desc = ckabs(yystack.l_mark[-2].yexp);
			ckrel(&stabsym, yystack.l_mark[0].yexp);
			goto writestab;
		}
	}
break;
case 27:
#line 274 "mas0.y"
	{
		nstabs++;
		if (passno==2) {
			strncpy(stabsym.n_name, "", NCPS);
			stabsym.n_dtype = ckabs(yystack.l_mark[-4].yexp);
			stabsym.n_desc = ckabs(yystack.l_mark[-2].yexp);
			ckrel(&stabsym, yystack.l_mark[0].yexp);
			goto writestab;
		}
	}
break;
case 28:
#line 284 "mas0.y"
	{
		nstabs++;
		if (passno==2) {
			strncpy(stabsym.n_name, "", NCPS);
			stabsym.n_dtype = ckabs(yystack.l_mark[-2].yexp);
			stabsym.n_desc = ckabs(yystack.l_mark[0].yexp);
			ckrel(&stabsym, dotp);
		writestab:
			fwrite((char *)&stabsym, sizeof(stabsym), 1, stabfil);
		}
	}
break;
case 31:
#line 299 "mas0.y"
	{
		yystack.l_mark[0].yname->n_type |= N_EXT;
		}
break;
case 32:
#line 302 "mas0.y"

		yystack.l_mark[0].yname->n_type |= N_EXT;
break;
case 33:
#line 305 "mas0.y"
	{
		yyval.yint = yystack.l_mark[0].yint;
	}
break;
case 34:
#line 308 "mas0.y"
	{
		yyval.yint = 0;
	}
break;
case 35:
#line 313 "mas0.y"
	{
		yyval.yint = ICOMM;
	}
break;
case 36:
#line 316 "mas0.y"
	{
		yyval.yint = ILCOMM;
	}
break;
case 40:
#line 325 "mas0.y"
	{
		i = curlen;
		pval = yystack.l_mark[0].yexp;
		flushfield(curlen);
		goto outx;
	}
break;
case 41:
#line 331 "mas0.y"
	{
		if (yystack.l_mark[-2].yexp->xtype != N_ABS)
			yyerror("Width expression not absolute");
		i = yystack.l_mark[-2].yexp->xvalue;
		pval = yystack.l_mark[0].yexp;
		if (bitoff+i > curlen)
			flushfield(curlen);
		if (i > curlen)
			yyerror("Expression crosses field boundary");
	outx:
		 if ((pval->xtype&XTYPE)!=N_ABS && passno==2) {
			if (curlen==NBPW/2 && bitoff==0) {
				outhw((short)pval->xvalue, pval->xtype, pval->xname, 0);
				dotp->xvalue += 2;
			} else if (curlen!=NBPW || bitoff) {
				yyerror("Illegal relocation in field");
				bitoff += i;
			} else {
				output(pval->xvalue, pval->xtype, pval->xname);
				dotp->xvalue += 4;
			}
		} else {
			if (i<NBPW) {
				li = pval->xvalue & ((1L<<i)-1);
				bitoff += i;
				ii = NBPW-bitoff;
				bitfield |= li << ii;
			} else {
				bitfield = pval->xvalue;
				bitoff = NBPW;
			}
		}
		ap = arglist;
		xp = explist;
	}
break;
case 42:
#line 367 "mas0.y"
	{
		curlen = NBPW/4;
		dotp->xvalue &= ~01;
	}
break;
case 43:
#line 372 "mas0.y"
	{
		curlen = NBPW;
		align(FW);
	}
break;
case 44:
#line 377 "mas0.y"
	{
		curlen = NBPW/2;
		align(HW);
	}
break;
case 45:
#line 382 "mas0.y"
	{
		ap->atype = AEXP;
		ap->xp = yystack.l_mark[0].yexp;
		ap->areg1 = 0;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 46:
#line 389 "mas0.y"
	{
		ap->atype = AREG;
		ap->areg1 = yystack.l_mark[0].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 47:
#line 395 "mas0.y"
	{
		ap->atype = AIREG;
		ap->areg1 = yystack.l_mark[-1].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 48:
#line 401 "mas0.y"
	{
		ap->atype = ADEC;
		ap->areg1 = yystack.l_mark[-1].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 49:
#line 407 "mas0.y"
	{
		ap->atype = AINC;
		ap->areg1 = yystack.l_mark[-2].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 50:
#line 413 "mas0.y"
	{
		ap->atype = AOFF;
		ap->xp = yystack.l_mark[-3].yexp;
		ap->areg1 = yystack.l_mark[-1].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 51:
#line 420 "mas0.y"
	{
		ap->atype = APIC;
		ap->xp = yystack.l_mark[-3].yexp;
		ap->areg1 = yystack.l_mark[-1].yint;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 52:
#line 427 "mas0.y"
	{
		ap->atype = ANDX;
		ap->xp = yystack.l_mark[-6].yexp;
		ap->areg1 = yystack.l_mark[-4].yint;
		ap->areg2 = yystack.l_mark[-2].yint;
		ap->asize = yystack.l_mark[-1].yint;
		yyval.yarg = ap++;
	}
break;
case 53:
#line 435 "mas0.y"
	{
		ap->atype = API2;
		ap->xp = yystack.l_mark[-6].yexp;
		ap->areg1 = yystack.l_mark[-4].yint;
		ap->areg2 = yystack.l_mark[-2].yint;
		ap->asize = yystack.l_mark[-1].yint;
		yyval.yarg = ap++;
	}
break;
case 54:
#line 443 "mas0.y"
	{
		ap->atype = AIMM;
		ap->xp = yystack.l_mark[0].yexp;
		ap->areg1 = 0;
		ap->areg2 = 0;
		yyval.yarg = ap++;
	}
break;
case 55:
#line 453 "mas0.y"
	{
		if ( (yystack.l_mark[0].yname->n_type&XTYPE)!=N_ABS || yystack.l_mark[0].yname->n_value<0 || yystack.l_mark[0].yname->n_value>SRREG)
			if (passno!=1)
				yyerror("Illegal register");
		yyval.yint = yystack.l_mark[0].yname->n_value;
	}
break;
case 57:
#line 464 "mas0.y"
	{
		yyval.yexp = xp++;
		yyval.yexp->xtype = yystack.l_mark[0].yname->n_type;
		if ((yystack.l_mark[0].yname->n_type&XTYPE)==N_UNDF) {
			yyval.yexp->xname = yystack.l_mark[0].yname;
			yyval.yexp->xvalue = 0;
			if (passno==1)
				yystack.l_mark[0].yname->n_type |= XFORW;
		} else {
			yyval.yexp->xvalue = yystack.l_mark[0].yname->n_value;
			yyval.yexp->xname = NULL;
			yyval.yexp->xloc = yystack.l_mark[0].yname->index;
		}
	}
break;
case 58:
#line 478 "mas0.y"
	{
		yyval.yexp = xp++;
		yyval.yexp->xtype = N_ABS;
		yyval.yexp->xvalue = * (long *)yystack.l_mark[0].ynumber;
		yyval.yexp->xloc = 0;
	}
break;
case 59:
#line 484 "mas0.y"
	{
		yyval.yexp = yystack.l_mark[-1].yexp;
	}
break;
case 60:
#line 487 "mas0.y"
	{
		goto comb;
	}
break;
case 61:
#line 490 "mas0.y"
	{
		goto comb;
	}
break;
case 62:
#line 493 "mas0.y"
	{
		goto comb;
	}
break;
case 63:
#line 496 "mas0.y"
	{
	comb:
		yyval.yexp = combine(yystack.l_mark[-1].yint, yystack.l_mark[-2].yexp, yystack.l_mark[0].yexp);
	}
break;
case 64:
#line 500 "mas0.y"
	{
		xp->xtype = N_ABS;
		xp->xvalue = 0;
		yyval.yexp = combine(yystack.l_mark[-1].yint, xp++, yystack.l_mark[0].yexp);
	}
break;
case 65:
#line 505 "mas0.y"
	{
		yyval.yexp = yystack.l_mark[0].yexp;
		yyval.yexp->xvalue = ~yyval.yexp->xvalue;
	}
break;
#line 1191 "y.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack))
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (short) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    yyerror("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
