CFLAGS = -I../../include -m32 -Wno-return-type

all:	mcc mnm mar msize mstrip

mcc:	mcc.c
	$(CC) $(CFLAGS) -o mcc mcc.c

mnm:	mnm.c
	$(CC) $(CFLAGS) -o mnm mnm.c

mar:	mar.c
	$(CC) $(CFLAGS) -o mar mar.c

msize:	msize.c
	$(CC) $(CFLAGS) -o msize msize.c

mstrip:	mstrip.c
	$(CC) $(CFLAGS) -o mstrip mstrip.c

install:	mcc mnm mar msize mstrip
	cp $^ ../../bin

clean:
	rm -f *.o mcc mnm mar msize mstrip core
